# Weather Forecast Application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

- Node version: 12.13.0 (used)

## Instructions

- Run `npm install` to install all dependencies.

- Run `ng serve` or `npm run start` to start the application. Navigate to `http://localhost:4200/`.


## Informations

- Tested for Chrome, Firefox, Edge. No support for IE11.
- Tested for different resolutions.
- Chrome is recommended for a better user experience.
