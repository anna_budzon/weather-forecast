import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search/services/search.service';
import { Forecast } from '../model/forecast';
import { City } from '../model/city';
import { FiveDaysForecast } from '../model/five-days-forecast.interface';
import { filter, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-forecasts',
  templateUrl: './forecasts.component.html',
  styleUrls: [ './forecasts.component.scss' ],
  animations: [
    trigger(
      'fadeIn',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('1s ease-out',
              style({ opacity: 1 }))
          ]
        )
      ]
    )
  ]
})
export class ForecastsComponent implements OnInit {
  city: City;
  forecasts: Array<Forecast>;
  isLoading: Observable<boolean>;

  constructor(private searchService: SearchService) {}

  ngOnInit(): void {
    this.isLoading = this.searchService.isLoading();
    this.loadFiveDaysForecast();
  }

  private loadFiveDaysForecast() {
    this.searchService.getFiveDaysForecast()
        .pipe(
          tap((fiveDaysForecast) => {
            if (!fiveDaysForecast) {
              this.forecasts = [];
            }
          }),
          filter(Boolean)
        )
        .subscribe(({ city, forecasts }: FiveDaysForecast) => {
          this.city = city;
          this.forecasts = forecasts;
        });
  }
}
