import { Component, Input } from '@angular/core';
import { Forecast } from '../../model/forecast';
import { MatDialog } from '@angular/material/dialog';
import { ForecastDetailsComponent } from '../forecast-details/forecast-details.component';

@Component({
  selector: 'app-forecast-card',
  templateUrl: './forecast-card.component.html',
  styleUrls: [ './forecast-card.component.scss' ]
})
export class ForecastCardComponent {

  @Input() forecast: Forecast;

  constructor(private dialog: MatDialog) {}

  openDetails(): void {
    this.dialog.open(ForecastDetailsComponent, {
      width: '460px',
      minHeight: '540px',
      hasBackdrop: true,
      data: this.forecast
    });
  }
}
