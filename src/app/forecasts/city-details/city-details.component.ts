import { Component, Input } from '@angular/core';
import { City } from '../../model/city';
import { faSun, faMoon, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: [ './city-details.component.scss' ]
})
export class CityDetailsComponent {

  currentDate: Date = new Date(Date.now());
  sunriseIcon = faSun;
  sunsetIcon = faMoon;
  location = faMapMarkerAlt;

  @Input() city: City;
}
