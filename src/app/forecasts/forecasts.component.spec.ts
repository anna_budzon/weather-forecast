import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastsComponent } from './forecasts.component';
import { ForecastCardComponent } from './forecast-card/forecast-card.component';
import { CityDetailsComponent } from './city-details/city-details.component';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { ForecastsSummaryComponent } from './forecasts-summary/forecasts-summary.component';
import { ForecastDetailsComponent } from './forecast-details/forecast-details.component';
import { ForecastsChartComponent } from './forecasts-chart/forecasts-chart.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('ForecastsComponent', () => {
  let component: ForecastsComponent;
  let fixture: ComponentFixture<ForecastsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ForecastsComponent,
        ForecastCardComponent,
        CityDetailsComponent,
        CurrentWeatherComponent,
        ForecastsSummaryComponent,
        ForecastDetailsComponent,
        ForecastsChartComponent
      ],
      imports: [
        HttpClientTestingModule,
        MatIconModule,
        MatDividerModule,
        MatButtonModule,
        MatGridListModule,
        MatDialogModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatSnackBarModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
