import { ChartOptions } from 'chart.js';
import { Color } from 'ng2-charts';

const fontFamily = 'Lato, sans-serif';
const fontColor = '#fff';
const gridLines = {
  zeroLineColor: 'rgba(255, 255, 255, 0.2)',
  color: 'rgba(255, 255, 255, 0.1)'
};

export const CHART_CONFIG: ChartOptions = {
  responsive: true,
  legend: {
    labels: {
      fontColor,
      fontFamily
    }
  },
  scales: {
    xAxes: [
      {
        type: 'time',
        gridLines,
        ticks: {
          source: 'data',
          autoSkip: true,
          maxTicksLimit: 20,
          fontFamily,
          fontColor
        },
        time: {
          tooltipFormat: 'ddd, D MMM h:mma',
          displayFormats: {
            hour: 'ddd, h:mm a'
          }
        }
      }
    ],
    yAxes: [
      {
        id: 'temperature-axis',
        position: 'left',
        gridLines,
        scaleLabel: {
          display: true,
          labelString: 'Temperature [°C]',
          fontColor,
          fontFamily
        },
        ticks: {
          fontColor,
          fontFamily
        }
      },
      {
        id: 'rain-axis',
        position: 'right',
        gridLines,
        scaleLabel: {
          display: true,
          labelString: 'Rain [mm]',
          fontColor,
          fontFamily
        },
        ticks: {
          fontColor
        }
      }
    ],
  },
  tooltips: {
    bodyFontFamily: fontFamily,
    titleFontFamily: fontFamily,
    footerFontFamily: fontFamily,
    callbacks: {
      label: (tooltipItem) => {
        return !tooltipItem.datasetIndex ?
          `Temperature: ${ tooltipItem.value }°C` :
          `Rain: ${ tooltipItem.value }mm`;
      }
    }
  }
};

export const CHART_COLORS: Array<Color> = [
  {
    backgroundColor: 'rgba(205, 248, 228, 0.15)',
    borderColor: 'rgba(83, 197, 164, 0.7)',
    pointBackgroundColor: 'rgba(83, 197, 164, 1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(83, 197, 164, 0.8)'
  },
  {
    backgroundColor: 'rgba(7, 76 ,78, 0.5)',
    borderColor: 'rgba(42, 103, 115, 0.8)',
    pointBackgroundColor: 'rgba(7, 76, 78, 0.6)',
    pointBorderColor: '#cfcfcf',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgb(59, 128, 132)'
  }
];
