import { Component, Input } from '@angular/core';
import { Forecast } from '../../model/forecast';
import { Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { CHART_COLORS, CHART_CONFIG } from './chart-config';

@Component({
  selector: 'app-forecasts-chart',
  templateUrl: './forecasts-chart.component.html',
  styleUrls: [ './forecasts-chart.component.scss' ]
})
export class ForecastsChartComponent {

  private _forecasts: Array<Forecast> = [];
  lineChartData: ChartDataSets[] = [];
  lineChartOptions: ChartOptions = CHART_CONFIG;
  lineChartColors: Color[] = CHART_COLORS;

  @Input() set forecasts(forecasts: Array<Forecast>) {
    this._forecasts = forecasts;
    this.lineChartData = [
      {
        data: forecasts.map(({ details: { temperature }, date }) => ({
          x: date,
          y: temperature
        })),
        label: 'Temperature'
      },
      {
        data: forecasts.map(({ rain = 0, date }) => ({
          x: date,
          y: rain
        })),
        label: 'Rain'
      }
    ];
  }

  get forecasts(): Array<Forecast> {
    return this._forecasts;
  }
}
