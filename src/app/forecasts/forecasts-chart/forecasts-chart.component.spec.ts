import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastsChartComponent } from './forecasts-chart.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartsModule } from 'ng2-charts';

describe('ForecastsChartComponent', () => {
  let component: ForecastsChartComponent;
  let fixture: ComponentFixture<ForecastsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastsChartComponent ],
      imports: [
        HttpClientTestingModule,
        ChartsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
