import { Component, Input } from '@angular/core';
import { Forecast } from '../../model/forecast';

@Component({
  selector: 'app-forecasts-summary',
  templateUrl: './forecasts-summary.component.html',
  styleUrls: ['./forecasts-summary.component.scss']
})
export class ForecastsSummaryComponent {
  averageTemperature: number;
  averagePressure: number;
  averageHumidity: number;
  averageWind: number;
  averageRain: number;
  averageSnow: number;
  minTemperature: number;
  maxTemperature: number;

  @Input() set forecasts(forecasts: Array<Forecast>) {
    const summary = this.getSummary(forecasts);
    this.averageTemperature = summary.tempSum / forecasts.length;
    this.averagePressure = summary.pressSum / forecasts.length;
    this.averageHumidity = summary.humidSum / forecasts.length;
    this.averageWind = summary.windSum / forecasts.length;
    this.averageRain = summary.rainSum ? (summary.rainSum / forecasts.length) : 0;
    this.averageSnow = summary.snowSum ? (summary.snowSum / forecasts.length) : 0;
    this.minTemperature = Math.round(summary.tempMin);
    this.maxTemperature = Math.round(summary.tempMax);
  }

  private getSummary(forecasts: Array<Forecast>) {
    return forecasts.reduce(
      (oldValue, {details, wind, rain = 0, snow = 0}) => {
        return {
          tempSum: oldValue.tempSum + details.temperature,
          humidSum: oldValue.humidSum + details.humidity,
          pressSum: oldValue.pressSum + details.pressure,
          windSum: oldValue.windSum + wind.speed,
          rainSum: oldValue.rainSum + rain,
          snowSum: oldValue.snowSum + snow,
          tempMin: Math.min(oldValue.tempMin, details.tempMin),
          tempMax: Math.max(oldValue.tempMax, details.tempMax),
        };
      }, {
        tempSum: 0,
        pressSum: 0,
        humidSum: 0,
        windSum: 0,
        rainSum: 0,
        snowSum: 0,
        tempMin: 100,
        tempMax: -100
      });
  }
}
