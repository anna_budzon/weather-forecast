import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastsSummaryComponent } from './forecasts-summary.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';

describe('ForecastsSummaryComponent', () => {
  let component: ForecastsSummaryComponent;
  let fixture: ComponentFixture<ForecastsSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastsSummaryComponent ],
      imports: [
        HttpClientTestingModule,
        MatListModule,
        MatDividerModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastsSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
