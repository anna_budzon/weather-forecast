import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Forecast } from '../../model/forecast';
import {
  faThermometerHalf,
  faTemperatureHigh,
  faTemperatureLow,
  faWind,
  faCloud,
  faCloudRain,
  faSnowflake
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-forecast-details',
  templateUrl: './forecast-details.component.html',
  styleUrls: [ './forecast-details.component.scss' ]
})
export class ForecastDetailsComponent {

  temp = faThermometerHalf;
  minTemp = faTemperatureLow;
  maxTemp = faTemperatureHigh;
  wind = faWind;
  cloud = faCloud;
  rain = faCloudRain;
  snow = faSnowflake;

  constructor(public dialogRef: MatDialogRef<ForecastDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public forecast: Forecast) {}

  onClose() {
    this.dialogRef.close();
  }
}
