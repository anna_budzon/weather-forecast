import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForecastsComponent } from './forecasts.component';
import { ForecastCardComponent } from './forecast-card/forecast-card.component';
import { MatIconModule } from '@angular/material/icon';
import { CityDetailsComponent } from './city-details/city-details.component';
import { MatDividerModule } from '@angular/material/divider';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { MatButtonModule } from '@angular/material/button';
import { ForecastsSummaryComponent } from './forecasts-summary/forecasts-summary.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { ForecastDetailsComponent } from './forecast-details/forecast-details.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { ForecastsChartComponent } from './forecasts-chart/forecasts-chart.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    ForecastsComponent,
    ForecastCardComponent,
    CityDetailsComponent,
    CurrentWeatherComponent,
    ForecastsSummaryComponent,
    ForecastDetailsComponent,
    ForecastsChartComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatDividerModule,
    MatButtonModule,
    MatGridListModule,
    MatDialogModule,
    MatListModule,
    MatProgressSpinnerModule,
    FontAwesomeModule,
    CarouselModule.forRoot(),
    ChartsModule
  ],
  exports: [
    ForecastsComponent
  ]
})
export class ForecastsModule {}
