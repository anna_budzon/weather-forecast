import { Component, Input } from '@angular/core';
import { SearchService } from '../../search/services/search.service';
import { Observable } from 'rxjs';
import { Forecast } from '../../model/forecast';
import { faTemperatureHigh, faTemperatureLow } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: [ './current-weather.component.scss' ]
})
export class CurrentWeatherComponent {

  currentWeather$: Observable<Forecast>;
  minTemp = faTemperatureLow;
  maxTemp = faTemperatureHigh;

  @Input() set cityId(id: number) {
    this.currentWeather$ = this.searchService.getCurrentWeatherByCity(id);
  }

  constructor(private searchService: SearchService) {}
}
