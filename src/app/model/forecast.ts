import { Weather, Wind } from './forecast-details';
import { CurrentWeatherApiResponse, ForecastFromApi } from './api-responses';

export class Forecast {
  date: Date;
  details: ForecastDetails;
  weather: Array<Weather>;
  clouds: number;
  wind: Wind;
  rain: number;
  snow: number;

  constructor(forecastFromApi: ForecastFromApi | CurrentWeatherApiResponse) {
    this.date = new Date(forecastFromApi.dt * 1000);
    this.details = {
      temperature: forecastFromApi.main.temp,
      feelsLike: forecastFromApi.main.feels_like,
      tempMin: forecastFromApi.main.temp_min,
      tempMax: forecastFromApi.main.temp_max,
      pressure: forecastFromApi.main.pressure,
      seaLevel: forecastFromApi.main.sea_level,
      groundLevel: forecastFromApi.main.grnd_level,
      humidity: forecastFromApi.main.humidity
    };
    this.clouds = forecastFromApi.clouds.all;
    this.wind = { ...forecastFromApi.wind };
    this.weather = [ ...forecastFromApi.weather ];
    this.rain = forecastFromApi.rain && forecastFromApi.rain['3h'];
    this.snow = forecastFromApi.snow && forecastFromApi.snow['3h'];
  }
}

export interface ForecastDetails {
  temperature: number;
  feelsLike: number;
  tempMin: number;
  tempMax: number;
  pressure: number;
  seaLevel: number;
  groundLevel: number;
  humidity: number;
}

