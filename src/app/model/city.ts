import { CityFromApi, Coordinates, ICity } from './api-responses';

export class City {
  sunriseDate: Date;
  sunsetDate: Date;

  constructor(public id: number,
              public name: string,
              public country: string,
              public coordinates: Coordinates,
              sunrise?: number,
              sunset?: number) {
    this.sunriseDate = sunrise && new Date(sunrise * 1000);
    this.sunsetDate = sunset && new Date(sunset * 1000);
  }

  static assign(city: ICity): City {
    return new City(
      city.id,
      city.name,
      city.country,
      city.coord,
      city.sunrise,
      city.sunset
    );
  }

  static assignMany(cities: Array<CityFromApi>): Array<City> {
    return cities.map((city: CityFromApi) =>
      new City(
        city.id,
        city.name,
        city.sys.country,
        city.coord)
    );
  }
}
