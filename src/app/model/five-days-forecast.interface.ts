import { Forecast } from './forecast';
import { City } from './city';

export interface FiveDaysForecast {
  forecasts: Array<Forecast>;
  city: City;
}
