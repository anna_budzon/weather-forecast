import { Clouds, ForecastDetails, Rain, Snow, Weather, Wind } from './forecast-details';

export interface CurrentWeatherApiResponse extends CityDetails, WeatherDetails {
  cod: number;
  message: string;
  base?: string;
  sys: {
    type: number;
    id: number;
    message: number;
    country: string;
    sunrise: number;
    sunset: number;
  };
}

export interface ForecastsApiResponse {
  cod: number;
  message: string;
  cnt: number;
  list: Array<ForecastFromApi>;
  city: ICity;
}

export interface CitiesApiResponse {
  cod: number;
  message: string;
  count: number;
  list: Array<CityFromApi>;
}

export interface ForecastFromApi extends WeatherDetails {
  sys: {
    pod: string;
  };
  dt_txt: string;
}

export interface CityFromApi extends CityDetails, WeatherDetails {
  sys: {
    country: string;
  };
}

export interface ICity extends CityDetails {
  country: string;
  state?: string;
  sunrise?: number;
  sunset?: number;
}

export interface CityDetails {
  id: number;
  name: string;
  coord: Coordinates;
  timezone?: number;
}

export interface WeatherDetails {
  dt: number;
  clouds: Clouds;
  wind: Wind;
  main: ForecastDetails;
  weather: Array<Weather>;
  rain: Rain;
  snow: Snow;
}

export interface Coordinates {
  lat: number;
  lon: number;
}

