import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SearchService } from './services/search.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { debounceTime, distinctUntilChanged, filter, switchMap, tap } from 'rxjs/operators';
import { CitiesService } from './services/cities.service';
import { City } from '../model/city';
import { of } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: [ './search.component.scss' ]
})
export class SearchComponent implements OnInit {

  cityFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3)
  ]);

  cities: Array<City>;
  isLoading: boolean = false;

  constructor(private searchService: SearchService,
              private citiesService: CitiesService) {}

  ngOnInit(): void {
    this.cityFormControl.valueChanges
      .pipe(
        filter((value) => typeof value !== 'object'),
        tap(() => this.isLoading = this.cityFormControl.valid),
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((value: string) =>
          this.cityFormControl.valid ?
            this.citiesService.getQueriedCities(value) : of([])
        ),
      )
      .subscribe((cities) => {
        this.isLoading = false;
        this.cities = cities;
      });
  }

  getCityName(city: City) {
    return city ? city.name : '';
  }

  onSearch() {
    if (this.cityFormControl.valid && !this.isCitySelected()) {
      this.searchService.getForecastsByCityName(this.cityFormControl.value)
          .subscribe();
    }
  }

  onSelectedChange({ option }: MatAutocompleteSelectedEvent): void {
    const selectedCity = option.value;
    this.searchService.getForecastsByCityId(selectedCity.id)
        .subscribe();
  }

  private isCitySelected() {
    return typeof this.cityFormControl.value === 'object';
  }
}
