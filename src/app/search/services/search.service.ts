import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Forecast } from '../../model/forecast';
import { FiveDaysForecast } from '../../model/five-days-forecast.interface';
import { City } from '../../model/city';
import { environment } from '../../../environments/environment';
import { CurrentWeatherApiResponse, ForecastsApiResponse } from '../../model/api-responses';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private forecastsURL = `${environment.BASE_URL}/forecast`;
  private currentWeatherURL = `${environment.BASE_URL}/weather`;

  private httpParams = new HttpParams()
    .set('appid', environment.API_KEY)
    .set('units', 'metric');

  private loading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private fiveDaysForecast: Subject<FiveDaysForecast | null> = new Subject<FiveDaysForecast | null>();

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar) {}

  getFiveDaysForecast(): Observable<FiveDaysForecast | null> {
    return this.fiveDaysForecast.asObservable();
  }

  isLoading(): Observable<boolean> {
    return this.loading.asObservable();
  }

  getCurrentWeatherByCity(cityId: number): Observable<Forecast> {
    return this.http.get<CurrentWeatherApiResponse>(
      `${ this.currentWeatherURL }?id=${ cityId }`, { params: this.httpParams }
      )
       .pipe(
         map((response: CurrentWeatherApiResponse) => new Forecast(response)),
         catchError(({ error }) => {
           this.snackBar.open(`${ error.cod } - Failed to retrieve current weather for specified city.`,
             null, {
               duration: 5000,
               panelClass: 'error-message'
             });
           return of(null);
         })
       );
  }

  getForecastsByCityName(cityName: string): Observable<Array<Forecast>> {
    return this.getForecasts(`q=${ cityName }`);
  }

  getForecastsByCityId(cityId: number): Observable<Array<Forecast>> {
    return this.getForecasts(`id=${ cityId }`);
  }

  private getForecasts(query: string): Observable<Array<Forecast>> {
    this.loading.next(true);
    return this.http.get<ForecastsApiResponse>(`${ this.forecastsURL }?${ query }`, { params: this.httpParams })
       .pipe(
         map((response: ForecastsApiResponse) => ({
           forecasts: response.list.map((forecastFromApi) => new Forecast(forecastFromApi)),
           city: City.assign(response.city)
         })),
         tap((forecasts: FiveDaysForecast) => this.fiveDaysForecast.next(forecasts)),
         tap(() => this.loading.next(false)),
         catchError(({ error }) => {
           this.loading.next(false);
           this.fiveDaysForecast.next(null);
           this.snackBar.open(`${ error.cod } - Failed to retrieve weather forecast for specified city.`,
             null, {
               duration: 5000,
               panelClass: 'error-message'
             });
           return of(null);
         })
       );
  }
}
