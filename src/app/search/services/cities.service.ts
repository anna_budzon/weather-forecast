import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { City } from '../../model/city';
import { Observable, of } from 'rxjs';
import { CitiesApiResponse } from '../../model/api-responses';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  citiesURL: string = `${environment.BASE_URL}/find`;

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar) {}

  getQueriedCities(inputValue: string): Observable<Array<City>> {

    return this.http.get<CitiesApiResponse>(`${this.citiesURL}?q=${inputValue}&appid=${environment.API_KEY}`)
       .pipe(
         map((response: CitiesApiResponse) => City.assignMany(response.list)),
         catchError(({ error }) => {
           this.snackBar.open(`${ error.cod } - Failed to retrieve the list of cities.`,
             null, {
               duration: 5000,
               panelClass: 'error-message'
             });
           return of([]);
         })
       );
  }
}
